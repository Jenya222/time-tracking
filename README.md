<div style="margin:0 auto"><img src="https://www.elegantthemes.com/blog/wp-content/uploads/2015/03/Time-tracking.jpg"></div>

# RESTful web service to automate time tracking

### Requirements

[docker](https://docs.docker.com/installation/#installation)

[docker-compose](https://docs.docker.com/compose/)

[Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Usage

```bash
    git clone git@gitlab.com:Jenya222/time-tracking.git
```

```bash
    touch nginx-error.log
    chmod 777 -R db/
    cd src/
    mkdir uploads    
    chmod 777 -R uploads
```

- All command has to run in the root directory.

- Install packages via composer

```bash
docker run --rm --interactive --tty \
    --volume $PWD:/app \
    composer install
```

- Run application:

```bash
docker-compose up -d
```

- Stop application:

```bash
docker-compose down
```

- Restart application:

```bash
docker-compose restart
```

## Methods
- **Project.** Assign employee to the project. If employee or project doesn't exist it will be created automatically.

    - POST Request:
        - http://your_server_ip/project
    - Parameters:
        - number = "employee number"
        - project_name = "project name"   
        
- **Login.** Start to tracking time. If employee doesn't exist it will be created automatically.

    - POST Request:
        - http://your_server_ip/login
    - Parameters:
        - number = "employee number"
        
- **Logout.** Finish to tracking time. 

    - POST Request:
        - http://your_server_ip/logout
    - Parameters:
        - number = "employee number"  
                    
- **Upload.** Uploading bulk records.  
  
    - POST Request:
        - http://your_server_ip/upload
    - Parameters:
        - csv = "link to a file" 
     
   - **Note!!!** File template is in the root directory.    
   - **Attention!!!** First of all employee has to been assigned to some project.    
- **Billable hours.** Returns how many hours spent on a project.  
  
    - GET Request:
        - http://your_server_ip/projects_hours?project_name=awesome_project
    - Parameters:
        - project_name = "project name"         
        
- **Peak Time.** Returns when the most people were working on the project in period of time.  

     - GET Request:
         - http://your_server_ip/peak_time?project_name=brilliant_project&date_from=2018-04-5&date_to=2018-04-13
     - Parameters:
         - project_name = "project name"        
         - date_from = "start period of time"        
         - date_to = "finish period of time"        

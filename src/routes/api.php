<?php
use Slim\Http\Request;
use Slim\Http\Response;
use \Src\Library\Controller as Controller;

$app = new \Slim\App;

$app->post('/login', function(Request $request, Response $response){
    $neededParam = ["number"];
    $controller = new Controller();
    $result = $controller->login($request, $response, $neededParam);

    return $result;
});

$app->post('/logout', function(Request $request, Response $response){
    $neededParam = ["number"];
    $controller = new Controller();
    $result = $controller->logout($request, $response, $neededParam);

    return $result;
});

$app->post('/upload', function(Request $request, Response $response){
    $neededParam = ["csv"];
    $controller = new Controller();
    $result = $controller->upload($request, $response, $neededParam);

    return $result;
});

$app->post('/project', function(Request $request, Response $response){
    $neededParam = ["number", "project_name"];
    $controller = new Controller();
    $result = $controller->project($request, $response, $neededParam);

    return $result;
});

$app->get('/projects_hours', function(Request $request, Response $response){
    $neededParam = ["project_name"];
    $controller = new Controller();
    $result = $controller->projectsHours($request, $response, $neededParam);

    return $result;
});

$app->get('/peak_time', function(Request $request, Response $response){
    $neededParam = ["project_name", "date_from", "date_to"];
    $controller = new Controller();
    $result = $controller->peakTime($request, $response, $neededParam);

    return $result;
});

$app->run();
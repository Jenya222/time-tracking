<?php
namespace Src\Library;

use Slim\Http\Request;
use Slim\Http\Response;
use \Src\Library\Helper as Helper;

class Controller
{
    /** response to client
     * @var array
     */
    public $answer = [];

    /** action Login
     * @param Request $request
     * @param Response $response
     * @param $neededParam
     * @return $result
     */
    public function login(Request $request, Response $response, $neededParam)
    {
        $allPostVars = $request->getParsedBody();
        $checkParams = Helper::hasItParameter($neededParam, $allPostVars);

        if ($checkParams["result"]) {
            $employeeID = Helper::getEmployeeID($allPostVars["number"]);

            if ($employeeID) {
                Helper::setLogin($employeeID);
                $this->answer = ["success" => "We have started to tracking employee with the number - '" . $allPostVars['number'] . "'"];
            } else {
                $newEmployee = Helper::setNewEmployee($allPostVars["number"]);

                if ($newEmployee) {
                    Helper::setLogin($newEmployee);
                    $this->answer = ["success" => "We created new employee with the number - '$newEmployee' and started to tracking his time"];
                }
            }
        } else {
            $this->answer = ["failure" => "You have to use the parameter: " . implode(", ", $checkParams)];
        }

        $result = $response->withHeader('Content-type', 'application/json')->withJson($this->answer);

        return $result;
    }

    /** action Logout
     * @param Request $request
     * @param Response $response
     * @param $neededParam
     * @return $result
     */
    public function logout(Request $request, Response $response, $neededParam)
    {
        $allPostVars = $request->getParsedBody();
        $checkParams = Helper::hasItParameter($neededParam, $allPostVars);

        if ($checkParams["result"]) {
            $employeeID = Helper::getEmployeeID($allPostVars["number"]);

            if ( $employeeID ) {
                Helper::setLogout($employeeID);
                $this->answer = ["success" => "We have finished to tracking employee with the number - '" . $allPostVars['number'] . "'"];
            } else {
                $this->answer = ["failure" => "We haven't tracked employee with the number - '" . $allPostVars['number'] . "'"];
            }
        } else {
            $this->answer = ["failure" => "You have to use the parameter: " . implode(", ", $checkParams)];
        }

        $result = $response->withHeader('Content-type', 'application/json')->withJson($this->answer);

        return $result;
    }

    /** action Upload
     * @param Request $request
     * @param Response $response
     * @param $neededParam
     * @return $result
     */
    public function upload(Request $request, Response $response, $neededParam)
    {
        $uploadedFiles = $request->getUploadedFiles();
        $checkParams = Helper::hasItParameter($neededParam, $uploadedFiles);

        if ($checkParams["result"]) {
            $uploadedFile = $uploadedFiles['csv'];

            if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
                $filename = Helper::moveUploadedFile($uploadedFile);
                $csv = Helper::parseUploadFile($filename);
                $this->answer = $csv;
            }

        } else {
            $this->answer = ["failure" => "You have to use the parameter: " . implode(", ", $checkParams)];
        }

        $result = $response->withHeader('Content-type', 'application/json')->withJson($this->answer);

        return $result;
    }

    /** action Project
     * @param Request $request
     * @param Response $response
     * @param $neededParam
     * @return $result
     */
    public function project(Request $request, Response $response, $neededParam)
    {
        $allPostVars = $request->getParsedBody();
        $checkParams = Helper::hasItParameter($neededParam, $allPostVars);

        if ($checkParams["result"]) {
            $employeeID = Helper::getEmployeeID($allPostVars["number"]);
            $projectID = Helper::getProjectID($allPostVars["project_name"]);

            if (!$projectID) {
                $projectID = Helper::setNewProject($allPostVars["project_name"]);
            }

            if (!$employeeID) {
                $employeeID = Helper::setNewEmployee($allPostVars["number"]);
                Helper::assignProject($projectID, $employeeID);
                $this->answer = ["success" => "We have created a new employee with number ". $allPostVars["number"] . " and successful assigned to the project '" . $allPostVars["project_name"] . "'"];
            } else {
                Helper::assignProject($projectID, $employeeID);
                $this->answer = ["success" => "Project '" . $allPostVars["project_name"] . "' successful assigned to employee with number " . $allPostVars["number"]];
            }
        } else {
            $this->answer = ["failure" => "You have to use the parameter: " . implode(", ", $checkParams)];
        }

        $result = $response->withHeader('Content-type', 'application/json')->withJson($this->answer);

        return $result;
    }

    /** action Projects_hours
     * @param Request $request
     * @param Response $response
     * @param $neededParam
     * @return $result
     */
    public function projectsHours(Request $request, Response $response, $neededParam)
    {
        $paramValue = $request->getParams();
        $checkParams = Helper::hasItParameter($neededParam, $paramValue);

        if ($checkParams["result"]) {
            $projectID = Helper::getProjectID($paramValue['project_name']);

            if ($projectID) {
                $projectHours = Helper::convertSeconds(Helper::getProjectsHours($projectID));
                $this->answer = ["success" => $projectHours];
            } else {
                $this->answer = ["failure" => "Check that you correct pointed out the project_name"];
            }
        } else {
            $this->answer = ["failure" => "You have to use the parameter: " . implode(", ", $checkParams)];
        }

        $result = $response->withHeader('Content-type', 'application/json')->withJson($this->answer);

        return $result;
    }

    /** action Peak_time
     * @param Request $request
     * @param Response $response
     * @param $neededParam
     * @return $result
     */
    public function peakTime(Request $request, Response $response, $neededParam)
    {
        $paramValue = $request->getParams();
        $checkParams = Helper::hasItParameter($neededParam, $paramValue);

        if ($checkParams["result"]){
            $projectID = Helper::getProjectID($paramValue['project_name']);

            if ($projectID) {
                $peakTime = Helper::getPeakTime($projectID, $paramValue['date_from'], $paramValue['date_to']);
                $this->answer = [
                    "success" => "The day when the most people were working on the " . $paramValue['project_name'] . " project was " . $peakTime["date"] . ". Then were working " . $peakTime["count"] . " employees: " . implode(", ", $peakTime["employees"])
                ];
            } else {
                $this->answer = ["failure" => "Check that you correct pointed out the project_name"];
            }
        } else {
            $this->answer = ["failure" => "You have to use the parameter: " . implode(", ", $checkParams)];
        }

        $result = $response->withHeader('Content-type', 'application/json')->withJson($this->answer);

        return $result;
    }
}
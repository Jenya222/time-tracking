<?php
namespace Src\Library;

use SQLite3;
use DateTime;
use Slim\Http\UploadedFile;


Class Helper
{
    /** db connect
     * @var
     */
    private static $db;

    /** upload directory
     * @var
     */
    private static $uploadDir = '/app/src/uploads';

    /** entry point to db
     * @return SQLite3
     */
    private static function getDB()
    {
        return self::$db = new SQLite3('../../db/tracking.db');
    }

    /** Get existing employee id
     * @param $number
     * @return array
     */
    public static function getEmployeeID($number)
    {
        $employeeID = [];
        $statement = self::getDB()->prepare('SELECT "id" FROM "employees" WHERE "number" = :number');
        $statement->bindValue(':number', $number, SQLITE3_INTEGER);
        $result = $statement->execute();
        $data = array();

        while ($output = $result->fetchArray(SQLITE3_ASSOC)) {
            $data[] = $output;
        }

        foreach ($data as $row) {
            $employeeID = $row['id'];
        }

        return $employeeID;
    }

    /** Get employee login time
     * @param $id
     * @return int
     */
    public static function getLoginTime($id)
    {
        $loginTime = 0;
        $statement = self::getDB()->prepare('SELECT "login", "logout" FROM "time_track" WHERE ("employee_id" = :employee_id AND "logout" IS NULL)');
        $statement->bindValue(':employee_id', $id);
        $result = $statement->execute();
        $data = array();

        while ($output = $result->fetchArray(SQLITE3_ASSOC)) {
            $data[] = $output;
        }

        foreach ($data as $row) {
            $loginTime = $row['login'];
        }

        return $loginTime;
    }

    /** Set timestamp where employee has started to work
     * @param $id
     */
    public static function setLogin($id)
    {
        $projectID = self::getAssignedProject($id);
        $statement = self::getDB()->prepare('INSERT INTO "time_track" ("login", "employee_id", "project_id") VALUES (:login, :employee_id, :project_id)');
        $statement->bindValue(':login', time());
        $statement->bindValue(':employee_id', $id);
        $statement->bindValue(':project_id', $projectID);
        $statement->execute();
    }

    /** Set timestamp where employee has finished to work
     * @param $id
     */
    public static function setLogout($id)
    {
        $loginTime = self::getLoginTime($id);
        $statement = self::getDB()->prepare('UPDATE "time_track" SET "logout" = :logout, "total_hours" = :hours WHERE "employee_id" = :employee_id AND "logout" IS NULL');
        $statement->bindValue(':logout', time());
        $statement->bindValue(':hours', time() - $loginTime);
        $statement->bindValue(':employee_id', $id);
        $statement->execute();
    }

    /** Set new employee and return his id
     * @param $number
     * @return int
     */
    public static function setNewEmployee($number)
    {
        $db = self::getDB();
        $statement = $db->prepare('INSERT INTO "employees" ("number") VALUES (:num)');
        $statement->bindValue(':num', $number, SQLITE3_INTEGER);
        $statement->execute();
        $newEmployeeID = $db->lastInsertRowID();

        return $newEmployeeID;
    }

    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param UploadedFile $uploaded file uploaded file to move
     * @return string filename of moved file
     */
    public static function moveUploadedFile(UploadedFile $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8));
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo(self::$uploadDir . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }

    /** Check the data before upload
     * @param $employeeNum
     * @param $login
     * @param $logout
     * @return bool
     */
    public static function uploadRules($employeeNum, $login, $logout)
    {
        $isRealEmployee = self::getEmployeeID($employeeNum);
        $projectID = self::getAssignedProject($isRealEmployee);

        if ($projectID && $isRealEmployee && ( strtotime($login) && strtotime($logout) != false)) {

            return true;
        } else {

            return false;
        }
    }

    /** Parse uploaded file and insert the data in time_track table
     * @param $filename
     * @return array
     */
    public static function parseUploadFile($filename)
    {
        $statement = self::getDB()->prepare(
            'INSERT INTO "time_track" ("login", "logout", "total_hours", "employee_id", "project_id") VALUES (:login, :logout, :hours, :employee_id, :project_id)'
        );
        $output = [];
        $result = ["success" => "All data has been uploading"];
        if (($handle = fopen(self::$uploadDir . DIRECTORY_SEPARATOR . $filename, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                for ($c = 0; $c < $num; $c++) {
                    $row = explode(";", $data[$c]);
                    $output[] = [
                        "login" => $row[0],
                        "logout" => $row[1],
                        "employee_number" => $row[2]];
                }
            }
            fclose($handle);
            array_shift($output);
        }

        foreach ($output as $record) {
            if (Helper::uploadRules($record['employee_number'], $record['login'], $record['logout'])) {
                $statement->bindValue(':login', strtotime($record['login']));
                $statement->bindValue(':logout', strtotime($record['logout']));
                $statement->bindValue(':hours', strtotime($record['logout']) - strtotime($record['login']));
                $statement->bindValue(':employee_id', self::getEmployeeID($record['employee_number']));
                $statement->bindValue(':project_id', self::getAssignedProject(self::getEmployeeID($record['employee_number'])));
                $statement->execute();
            } else {
                $result = ["failure" => "Check date format and employee number"];
            }
        }

        return $result;
    }

    /** Assign project to employee
     * @param $projectID
     * @param $number
     */
    public static function assignProject($projectID, $number)
    {
        $statement = self::getDB()->prepare('UPDATE "employees" SET "projects_id" = :projects_id WHERE "id" = :employee_id');
        $statement->bindValue(':projects_id', $projectID);
        $statement->bindValue(':employee_id', $number);
        $statement->execute();
    }

    /** Get project assigned to employee
     * @param $id
     * @return int
     */
    public static function getAssignedProject($id)
    {
        $projectID = 0;
        $statement = self::getDB()->prepare('SELECT "projects_id" FROM "employees" WHERE "id" = :id');
        $statement->bindValue(':id', $id);
        $result = $statement->execute();
        $data = array();

        while($output = $result->fetchArray(SQLITE3_ASSOC)) {
            $data[] = $output;
        }

        foreach ($data as $row) {
            $projectID = $row['projects_id'];
        }

        return $projectID;
    }

    /** Set new project
     * @param $project_name
     * @return int
     */
    public static function setNewProject($project_name)
    {
        $db = self::getDB();
        $statement = $db->prepare('INSERT INTO "projects" ("project_name") VALUES (:project_name)');
        $statement->bindValue(':project_name', $project_name);
        $statement->execute();
        $newProjectID = $db->lastInsertRowID();

        return $newProjectID;
    }

    /** Get project id in projects table
     * @param $project_name
     * @return array
     */
    public static function getProjectID($project_name)
    {
        $projectID = [];
        $statement = self::getDB()->prepare('SELECT "id" FROM "projects" WHERE "project_name" = :project_name');
        $statement->bindValue(':project_name', $project_name);
        $result = $statement->execute();
        $data = array();

        while ($output = $result->fetchArray(SQLITE3_ASSOC)) {
            $data[] = $output;
        }

        foreach ($data as $row) {
            $projectID = $row['id'];
        }

        return $projectID;
    }

    /** Get projects hours
     * @param $projectID
     * @return int
     */
    public static function getProjectsHours($projectID)
    {
        $hours = 0;
        $statement = self::getDB()->prepare(
            'SELECT SUM(total_hours) FROM time_track LEFT JOIN projects ON time_track.project_id = projects.id WHERE time_track.project_id = :project_id'
        );
        $statement->bindValue(':project_id', $projectID);
        $result = $statement->execute();
        $data = array();

        while ($output = $result->fetchArray(SQLITE3_ASSOC)) {
            $data[] = $output;
        }

        foreach ($data as $row) {
            $hours = $row['SUM(total_hours)'];
        }

        return $hours;
    }

    /** Get the dat when the most people were working on given project and date period
     * @param $projectID
     * @param $dateFrom
     * @param $dateTo
     * @return array
     */
    public static function getPeakTime($projectID, $dateFrom, $dateTo)
    {
        $statement = self::getDB()->prepare(
            'SELECT DISTINCT employees.number, time_track.login FROM time_track LEFT JOIN employees ON time_track.employee_id = employees.id WHERE time_track.login >= :dateFrom AND time_track.login <= :dateTo AND time_track.project_id = :project_id'
        );
        $statement->bindValue(':dateFrom', strtotime($dateFrom));
        $statement->bindValue(':dateTo', strtotime($dateTo));
        $statement->bindValue(':project_id', $projectID);
        $result = $statement->execute();
        $data = array();

        while ($output = $result->fetchArray(SQLITE3_ASSOC)) {
            $data[] = $output;
        }

        $statOfPeriod = [];
        foreach ($data as $row) {
            $statOfPeriod[date("Y-m-d", $row["login"])][] = $row["number"];
        }

        $arraySize = 0;
        foreach ($statOfPeriod as $key => $array) {
            if (count($array) > $arraySize) {
                $arraySize = count($array);
                $largestArray = [
                    "count" => $arraySize,
                    "date" => $key,
                    "employees" => $array
                ];
            }
        }

        return $largestArray;
    }

    /** Validate input parameters
     * @param array $needed
     * @param array $input
     * @return array
     */
    public static function hasItParameter( $needed = array(), $input = array() )
    {
        $result = [];
        foreach ($needed as $item) {
            if (!array_key_exists($item, $input)) {
                array_push($result, $item);
            }
        }

        if (count($result) == 0) {
            $result = [ "result" => true ];

            return $result;
        } else {

            return $result;
        }
    }

    /** Convert seconds to readable format
     * @param $seconds
     * @return string
     */
    public static function convertSeconds($seconds)
    {
        $dt1 = new DateTime("@0");
        $dt2 = new DateTime("@$seconds");

        return $dt1->diff($dt2)->format('%a days, %h hours, %i minutes and %s seconds');
    }
}